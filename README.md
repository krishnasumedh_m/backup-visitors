--> Extended the functionalities of the student backup system.  ( https://bitbucket.org/krishnasumedh_m/backup-system-for-course-registration )

--> Designed and implemented visitors to the main and backup trees using the visitor design pattern.

--> Added additional functionalities such as statistics computation, similarity based grouping etc. to the trees using the Visitor Implementation classes.    
    ( https://en.wikipedia.org/wiki/Visitor_pattern  ) . 


