package backupVisitors.util;
import java.util.ArrayList;
import backupVisitors.myTree.Node;
import backupVisitors.myTree.Tree;
import backupVisitors.myTree.Updates;
import backupVisitors.visitor.FullTimeStatusVisitorImpl;
import backupVisitors.visitor.IdenticalRecordsVisitorImpl;
import backupVisitors.visitor.SortedRecordsVisitorImpl;
import backupVisitors.visitor.StatsVisitorImpl;

enum courseConstants
{
	A,B,C,D,E,F,G,H,I,J,K
};


public class TreeBuilder  
{
	String line = null;
	//construct three Tree instances
	Tree t1 = new Tree();
	Tree t2 = new Tree();
	Tree t3 = new Tree();
	ArrayList <Node> arl = new ArrayList <Node> ();
	Node Node_orig;
	Node backup_Node_1  ;
    Node backup_Node_2 ;
    Node root1; 
    Node root2;
    Node root3; 
    Updates up = new Updates();
    Results r1 = new Results();
    Results r2 = new Results();
    Results r3 = new Results();
    
	
	public void readL(FileProcessor fp) 
	{
		
		while( null != ( line = fp.readLine() ) )
	    {
			 
			 String[] arr = line.split(":");    
		     int bn =  Integer.parseInt(arr[0]); 
		     String course = arr[1];
		      
		     //get root of the tree
		      root1 = t1.getRoot();
		      root2 = t2.getRoot();
		      root3 = t3.getRoot();
		     
		     // Search if BNumber containing node exists.
		     Node result1 = t1.searchNode(root1, bn);
		     Node result2 = t2.searchNode(root2, bn);
		     Node result3 = t3.searchNode(root3, bn);
		     
		     if(result1 == null && result2 == null && result3 == null)
		     {
		    	 // System.out.println("Not found");
		    	  Node_orig = new Node(bn,course);
	//	    	  System.out.println("0rig node"+ Node_orig.getBNumber() +"  "+ Node_orig.getCourses());  
		    	  try 
		    	  {
					backup_Node_1  =(Node)Node_orig.clone();				
					backup_Node_2  =(Node)Node_orig.clone();
					
				  } 
		    	  catch (CloneNotSupportedException e) 
		    	  {	
					e.printStackTrace();
				  }
		    	  
			      Node_orig.registerObserver(backup_Node_1);
			      Node_orig.registerObserver(backup_Node_2);
			     
			         //add the nodes to trees.
	     		     t1.insertNode(root1 ,Node_orig);
	
    			     t2.insertNode(root2 ,backup_Node_1);
   
    			     t3.insertNode(root3 ,backup_Node_2);
		    	 
		     }
		     else
		     {
		    	 //System.out.println("Node found"); 
		    	 //result1 is search result of found node
		    	 result1.addCourse(course);
		    	 up.setOperation("add");
		    	 up.setCourse(course);
		    	 result1.notifyObserver(up);
		     }
		     	
	    } 	
		//After insert and before delete
	   //   t1.traverseNode(r1,root1);
	  //	t2.traverseNode(r2,root2);	
	 //	    t3.traverseNode(r3,root3);		
	}
	
	
	
	public void readDelete(FileProcessor fp) 
	{
	
		while( null != ( line = fp.readLine() ) )
	    {
			  String[] arr = line.split(":");    
		      int bn =  Integer.parseInt(arr[0]);
		      String course = arr[1];
		      
		      Node delNode = t1.delete(root1,bn);
		      //If Node trying to be deleted is 
		      //not present in the tree. 
		      if(delNode == null)
		      {
		    	  System.out.println("Cannot find B_number with course -> " + line);
		    	  continue;
		      }
		      delNode.deleteCourse(course);
		      
		      up.setOperation("delete");
		      up.setCourse(course);
		      
		      delNode.notifyObserver(up);
		      
	    }
		/*
		System.out.println("\n-- After Delete -- ");
		t1.traverseNode(root1);
		System.out.println("\n-- ------ ");
		t2.traverseNode(root2);
		System.out.println("\n-- ------ ");
		t3.traverseNode(root3);
		*/
	        t1.printNodes(r1,root1);
    	    t2.printNodes(r2,root2);
            t3.printNodes(r3,root3);
			
	}
	
	
	public ArrayList<Results> getResultsInstance()
	{
		ArrayList <Results> ar = new ArrayList <Results> ();
		
		ar.add(r2);
		ar.add(r3);
		ar.add(r1);
		
	   return ar;	
		
	}
	
	public void visitor()
	{
		
	  r1  =  t1.accept(new FullTimeStatusVisitorImpl());
	  
	  r2 = t2.accept(new StatsVisitorImpl());
	  
	  r3 = t3.accept(new SortedRecordsVisitorImpl());
	  
	  r1 = t1.accept(new IdenticalRecordsVisitorImpl());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
