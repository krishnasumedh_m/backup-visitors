package backupVisitors.util;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileProcessor 
{

    String file = null;
    BufferedReader br;
    String line;
    
    public FileProcessor (String fname)
    {
       file = fname; 
       try
       {  
    	   File f = new File(file);
    	   if (f.length() == 0) 
    	   {
    		   System.err.println("File empty.");
       	       System.exit(0);
    	   } 
         FileReader fr = new FileReader(file);
         br = new BufferedReader(fr);
         
        // System.out.println(br.readLine());
         
       /*  if (br.readLine() == null) 
         {
        	    System.err.println("empty");
        	    System.exit(0);
         } */
       }
       
      catch(FileNotFoundException f)
      {
          System.err.println("File Not Found");
          System.exit(0);
      }
       
        
    }
  
    public  String readLine()
    { 
      try
      {    
        line = br.readLine();
      }
      catch(IOException exc)
      {
    	  System.err.println("IO Exception");
    	  exc.printStackTrace();
    	  System.exit(0);
      } 
      
     // returns string representation of the numbers present in the file.
      return line;
    }
}
