package backupVisitors.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Results implements FileDisplayInterface,StdoutDisplayInterface
{
	private ArrayList<String> ar = new ArrayList<String>(); 
    BufferedWriter bwrite= null;
    FileWriter fwrite = null;
    String result = "";
    
    
    public void storeNewResult(String s)
    {  
      //insert result strings into the arrayList
       ar.add(s);
    }
    
    public String getResult()
    {     	     
    	for (String res: ar)
    	{
            result = result + res + "\n";
        }    	
    	//returns all the result strings of the arrayList
    	return result;
    }
    
    @Override
    public void writeToFile(String s)
    {
    
    //write the strings into the file
    try
    {
    	fwrite = new FileWriter(s);
    	bwrite = new BufferedWriter (fwrite);
    	bwrite.write(result);
    }
    catch (IOException io) 
    {   System.err.println("error : Output file");
		io.printStackTrace();
		System.exit(0);
	} 
    
     finally 
     {
		try 
		{
			if (bwrite != null)
				bwrite.close();

			if (fwrite != null)
				fwrite.close();
		} 
		
		 catch (IOException e) 
		 {
			System.err.println("IOException: close()");
			e.printStackTrace();
			System.exit(0);
	     }
      }
    
    	
   }

    @Override
    public void writeToStdout(String s)
    {
    	// System.out.println(s);
    
    }


}

