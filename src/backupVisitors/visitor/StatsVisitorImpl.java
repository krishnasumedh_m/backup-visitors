package backupVisitors.visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import backupVisitors.myTree.Node;
import backupVisitors.myTree.Tree;
import backupVisitors.util.Results;

public class StatsVisitorImpl implements TreeVisitorI  
{
	Node root;
	Node node;
	int courseCount = 0;
	int studentCount = 0;
	float average = (float) 0.0;
    double median = 0.0;
	List<Integer> courses = new  ArrayList<> ();
	Results r1= new Results();
	
	@Override
	public Results visit(Tree t) 
	{
		root = t.getRoot();
		traverseNode( root);
		float d  = computeAverage();
		double m   = computeMedian();
		//System.out.println(" Average: "+ d+" Median:  "+m);
		r1.storeNewResult(" Average: "+ d+" Median:  "+m);
		r1.getResult();
		return r1;
	}
	
	
	public void traverseNode(Node node)
	{
		if(node.getLeftNode()!=null)
		{
			traverseNode(node.getLeftNode());	
		}
		
	    count(node.getBNumber(),node.getCourses());
		//System.out.println("B_Number "+ node.getBNumber()+"   Courses:"+node.getCourses());
		
		if(node.getRightNode()!=null)
		{
			traverseNode(node.getRightNode());	
		}
		
	}
	
	
	public void count(int Bno, Set<String> course)
	{
		int numberofcourses = 0;
       for(String str : course)
       { 
    	   if(str != "S")
    	   {
    		   //courseCount = courseCount +  1;
    		   numberofcourses = numberofcourses + 1;
    	   }   
       }
       
         courseCount = courseCount + numberofcourses ;
         courses.add(numberofcourses);
         studentCount = studentCount + 1;
	}	
	
	public float computeAverage()
	{
		average = (float)courseCount / studentCount;
		return average;
		 
	}
	
	public double computeMedian()
	{
		Collections.sort(courses);
		int size = courses.size();
		//   System.out.println("size:  "+size);
		//   System.out.println("courses:  "+courses);
		if(size %2 == 0)
		{
			
			median = (double)(courses.get((size/2)-1) + courses.get((size/2)-1 + 1) )/2 ;
		}
		else
		{
			median = courses.get( (size+1)/2 -1 );
		}
		
		return median;	
	}
	
	

}
