package backupVisitors.visitor;

import backupVisitors.myTree.Node;
import backupVisitors.myTree.Tree;
import backupVisitors.myTree.Updates;
import backupVisitors.util.Results;

public class FullTimeStatusVisitorImpl implements TreeVisitorI 
{

	Node root;
	Node node;
	Updates up = new Updates();
	Results r = new Results();
	
	@Override
	public Results visit(Tree t) 
	{
		root = t.getRoot();
		traverseNode( root);
		return r;
	}
	
	public void traverseNode(Node node)
	{
		if(node.getLeftNode()!=null)
		{
			traverseNode(node.getLeftNode());	
		}
	
		if ( node.getCourses().size()  < 3 )
		{ 
			
			 node.addCourse("S");
	    	 up.setOperation("add");
	    	 up.setCourse("S");
	    	 node.notifyObserver(up);
		    
		}	
		//System.out.println("B_Number "+ node.getBNumber()+"   Courses:"+node.getCourses());
		
		 //check observer node's content.
		// node.observersContent();
		
		if(node.getRightNode()!=null)
		{
			traverseNode(node.getRightNode());	
		}
		
	}
  
  	
}
