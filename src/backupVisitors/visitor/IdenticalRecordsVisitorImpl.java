package backupVisitors.visitor;

import java.util.ArrayList;
import java.util.Set;
import backupVisitors.myTree.Node;
import backupVisitors.myTree.Tree;
import backupVisitors.util.Results;

public class IdenticalRecordsVisitorImpl implements TreeVisitorI  
{
	Node root;
	Node node;
	Results r = new Results();
	ArrayList<Node> n;
	int i=1;
	
	@Override
	public Results visit(Tree t) 
	{
		root = t.getRoot();
		traverseNode(root);	
		r.getResult();
		return r;
		
	}
	
	
	public void traverseNode(Node node)
	{
		
		if(node.getLeftNode()!=null)
		{
			traverseNode(node.getLeftNode());	
		}
		
	  
		 n = new ArrayList<>();
		 traverse(root,node);
		 
		 if(n.size() != 0)
		 {	
		   r.storeNewResult("------->Group "+ i);	i++; 
		  // System.out.println("------->Group "+ i); 
		    for(Node no : n)
		    {
			//  System.out.println("B_Number "+ no.getBNumber()+"   Courses: "+no.getCourses());
			  r.storeNewResult("B_Number: "+ no.getBNumber()+"   Courses: "+no.getCourses() );
		    }
	
		}
		
		
		if(node.getRightNode()!=null)
		{
			traverseNode(node.getRightNode());	
		}
		
	}
	
	
	public void traverse(Node root,Node cnode)
	{
		if(root.getLeftNode()!=null)
		{
			traverse(root.getLeftNode(),cnode);	
		}
		
		if( (root.getSize() == cnode.getSize() ) && (root.getGrouped() == false) )
		{
		     compare(root,cnode);	
		}
		
		if(root.getRightNode()!=null)
		{
			traverse(root.getRightNode(),cnode);	
		}
		
	}
	
	
	public void compare(Node root, Node cnode)
	{
	Set<String> s1	= root.getCourses();
	Set<String> s2	= cnode.getCourses();
	if(s1.containsAll(s2) && s2.containsAll(s1))
	{
		root.setGrouped(true);
        n.add(root.getNode());		
		//r.storeNewResult("B_Number:"+ root.getBNumber()+"   Courses:"+ root.getCourses() );
		//System.out.println("B_Number "+ root.getBNumber()+"   Courses:"+root.getCourses());
		
	}
		
		
		
	}
	
	

}
