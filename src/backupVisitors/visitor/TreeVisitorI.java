package backupVisitors.visitor;
import backupVisitors.myTree.Tree;
import backupVisitors.util.Results;

public interface TreeVisitorI 
{
    public Results visit(Tree t);
}
