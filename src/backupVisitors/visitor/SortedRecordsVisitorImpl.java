package backupVisitors.visitor;

import backupVisitors.myTree.Node;
import backupVisitors.myTree.Tree;
import backupVisitors.util.Results;

public class SortedRecordsVisitorImpl implements TreeVisitorI  
{
	Node root;
	Node node;
	Results r = new Results();
	
	
	@Override
	public Results visit(Tree t) 
	{
		root = t.getRoot();
		traverseNode( root);	
		r.getResult();
		return r;
	}
	
	
	
	public void traverseNode(Node node)
	{
		
		if(node.getRightNode()!=null)
		{
			traverseNode(node.getRightNode());	
		}
		
	   r.storeNewResult("B_Number:"+ node.getBNumber()+"   Courses:"+ node.getCourses() ); 
	  // System.out.println("B_Number "+ node.getBNumber()+"   Courses:"+node.getCourses());
		
				
		if(node.getLeftNode()!=null)
		{
			traverseNode(node.getLeftNode());	
		}
		 
	}
	
	
	
	
	

}
