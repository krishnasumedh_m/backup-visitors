package backupVisitors.myTree;

import backupVisitors.util.Results;
import backupVisitors.visitor.TreeVisitorI;

public class Tree 
{
	Node Root  ;
	Node n ;
	
	public Results accept(TreeVisitorI ti)
	{
		Results r = ti.visit(this);
		return r;
	}
	
	
	public void setRoot(Node n)
	{
		Root = n;
	}
	
	public Node getRoot()
	{
		return Root;
	}
		
	public void traverseNode(Results r ,Node node)
	{
		if(node.getLeftNode()!=null)
		{
			traverseNode(r,node.leftnode);	
		}
		
		r.storeNewResult("B_Number:"+ node.getBNumber()+"   Courses:"+ node.getCourses() );
		
	//	System.out.println("B_Number "+ node.getBNumber()+"   Courses:"+node.getCourses());
		
		if(node.getRightNode()!=null)
		{
			traverseNode(r,node.rightnode);	
		}
		
	}
	
	public void printNodes(Results r,Node root)
	{
		traverseNode(r,root);
		
	}
	
	
	public Node delete(Node root, int bn)
	{
		return searchNode(root,bn);
			
	}
	
	
	public Node searchNode(Node Root, int Bnumber)
	{  		
		if(Root == null)
		{
	      n = Root;
	      return n;
		}
		
		 if(Root.getBNumber() == Bnumber )
		{
			n =  Root ;
		}
		
		 if(Root.getBNumber() < Bnumber)
		{
			return searchNode(Root.rightnode,Bnumber ) ;
		}
		
			if(Root.getBNumber() > Bnumber )
		{
			return searchNode(Root.leftnode,Bnumber ) ;
		}
					
		return n;	
	}
	
	
	
	public void insertNode(Node Root, Node node)
	{
		if(this.Root == null)
		{	 this.Root = node;
		  //   System.out.println("this.Root: "+ this.Root);
		 //     System.out.println("Root local : "+ Root);
		     setRoot(this.Root);
		     return;
		}
		
		if( Root.getBNumber() < node.getBNumber()  )
		   {
			    if(Root.rightnode == null )
			    {
			    	Root.rightnode = node;
			
			    }
			    else
			    {
			    	insertNode(Root.rightnode,node);  
			    }	
		   
		   }
		
		
		if(Root.getBNumber() > node.getBNumber())
		  {
	
			 if(Root.leftnode == null )
			    {
			    	Root.leftnode = node;
			    	
			    }
			    else
			    {
			    	insertNode(Root.leftnode,node);
			    	
			    }	
		  		
		  }
		
		
	}

	
	
	
	
}
