package backupVisitors.myTree;

public interface SubjectI 
{
	
    void registerObserver(ObserverI o);
    void removeObserver(ObserverI o);
    void notifyObserver(Object o);
     
    
}