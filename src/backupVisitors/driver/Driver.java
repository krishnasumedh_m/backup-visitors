package backupVisitors.driver;

import java.util.ArrayList;
import backupVisitors.util.FileDisplayInterface;
import backupVisitors.util.FileProcessor;
import backupVisitors.util.Results;
import backupVisitors.util.TreeBuilder;

public class Driver 
{
	String line = null;

	public static void main(String[] args) 
	{	
		ArrayList<Results> res;
		FileProcessor fp,fp1;
		FileDisplayInterface fdIn;
		  
		if( (args.length != 5))
		   {
			 System.err.println("Invalid Arguments! Format : <input.txt> <delete.txt> <stats.txt> <sorted.txt> <identical.txt> \n");
			 System.exit(0);
		   }
		
		 
		  TreeBuilder tb = new TreeBuilder();
		   
		  fp = new FileProcessor(args[0]);
		  tb.readL(fp);
		  
		  fp1 = new FileProcessor(args[1]);
		  tb.readDelete(fp1);
		  
          tb.visitor();
          
		  
		  int i=1;
		  res = tb.getResultsInstance();
		  
		  for(Results R :res)
		  {
			  fdIn = R;
			  i++;
			  
			  fdIn.writeToFile(args[i]);
			  if(i==5)
				  break;
		  }
		  

	}

}
